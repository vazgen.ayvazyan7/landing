import Vue from 'vue'
import App from './App.vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import VueMask from 'v-mask'
require('./assets/app.scss');

Vue.config.productionTip = false;
Vue.use(VueMask);

new Vue({
  render: h => h(App),
}).$mount('#app');
